title: Issues Sending DMs
date: 2017-12-17 16:24:00
resolved: true
resolvedWhen: 2017-12-17 16:58:00
# Possible severity levels: down, disrupted, notice
severity: disrupted
affected:
  - API
section: issue
